#!/bin/bash

# Define the pod name pattern
pod_pattern="myapp-deployment"

# Check if any pod matches the pattern
if minikube kubectl -- get pods | grep -q "$pod_pattern"; then
    echo "Pod with name matching '$pod_pattern' exists."

    # Execute kubectl port-forward command
   minikube kubectl -- port-forward --address 0.0.0.0 svc/myapp-service 8080:80 & disown

    echo "kubectl port-forward command executed successfully."
else
    echo "No pod found with name matching '$pod_pattern'."
fi

