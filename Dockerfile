#Use the official Nginx base image from Docker Hub

FROM nginx:latest

# Copy your custom configuration file (if needed)
# COPY nginx.conf /etc/nginx/nginx.conf

# (Optional) Copy your static website files to the Nginx document root
 COPY src /usr/share/nginx/html

# (Optional) Expose ports if necessary (e.g., if you modified the Nginx configuration)
 EXPOSE 80
# EXPOSE 443

# CMD instruction is not needed in this case as the base image provides a default CMD
# CMD ["nginx", "-g", "daemon off;"]

